﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using golfus;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetLeaderboard()
        {
            methods methods = new methods();
            string leaderboard = methods.getLeaderboard();
        }

        [TestMethod]
        public void Leaderboard()
        {
            methods methods = new methods();
            string leaderboardXml = methods.getLeaderboard();

            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(leaderboardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(leaderboardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                }
            }

            if (xmlDoc != null)
            {
                methods.Leaderboard(xmlDoc);
            }
        }
    }
}
