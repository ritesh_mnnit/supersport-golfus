﻿using golfus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;

namespace TestProject1
{
    /// <summary>
    ///This is a test class for Service1Test and is intended
    ///to contain all Service1Test Unit Tests
    ///</summary>
    [TestClass()]
    public class Service1Test
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for Leaderboard
        ///</summary>
        [TestMethod()]
        public void LeaderBoardTest()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            target.tour = ConfigurationManager.AppSettings["tour"];
            target.dbConn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);
            target.dbConn.Open();
            string leaderboardXml = target.getLeaderboard();
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(leaderboardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(leaderboardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                }
            }
            target.Leaderboard(xmlDoc);
            target.dbConn.Close();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Leaderboard
        ///</summary>
        [TestMethod()]
        public void LeaderBoard1Test()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            target.tour = ConfigurationManager.AppSettings["tour1"];
            target.dbConn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);
            target.dbConn.Open();
            string leaderboardXml = target.getLeaderboard1();
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(leaderboardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(leaderboardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                }
            }
            target.Leaderboard(xmlDoc);
            target.dbConn.Close();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Scorecard
        ///</summary>
        [TestMethod()]
        public void ScorecardTest()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            target.tour = ConfigurationManager.AppSettings["tour"];
            target.dbConn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);
            target.dbConn.Open();
            string scorecardXml = target.getScorecard();
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(scorecardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(scorecardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                }
            }
            target.Scorecard(xmlDoc);
            target.dbConn.Close();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Scorecard
        ///</summary>
        [TestMethod()]
        public void Scorecard1Test()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            target.tour = ConfigurationManager.AppSettings["tour1"];
            target.dbConn = new SqlConnection(ConfigurationManager.AppSettings["sql"]);
            target.dbConn.Open();
            string scorecardXml = target.getScorecard1();
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(scorecardXml))
            {
                xmlDoc = new XmlDocument();
                try
                {
                    xmlDoc.LoadXml(scorecardXml);
                }
                catch (Exception ex)
                {
                    xmlDoc = null;
                }
            }
            target.Scorecard(xmlDoc);
            target.dbConn.Close();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for getStats
        ///</summary>
        [TestMethod()]
        public void getStatsTest()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            string statsCode = "02395"; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.getStats(statsCode);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for stats
        ///</summary>
        [TestMethod()]
        public void stats()
        {
            Service1 target = new Service1(); // TODO: Initialize to an appropriate value
            string statsCode = "02395"; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.getStats(statsCode);
            XmlDocument xmlDoc = null;
            if (!String.IsNullOrEmpty(actual))
            {
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(actual);
            }
            target.Stats(xmlDoc, "fedex-cup");
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}