﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using log4net;

namespace golfus
{
    public class methods
    {
        public string getLeaderboard()
        {
            string xml = string.Empty;

            try
            {
                //WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=RSyndFullLeaderboard&action=Request&u=supersportfeed&p=gjg093j4trt49");
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=TSyndFullLeaderboard&action=Request&u=testingfeed&p=testingfeed1");
                myRequest.Proxy = returnProxy();
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                xml = string.Empty;
            }

            return xml;
        }

        private WebProxy returnProxy()
        {
            WebProxy tmpProxy = new WebProxy();
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            string proxyUsername = Sw.ReadLine();
            string proxyPassword = Sw.ReadLine();
            string proxyDomain = Sw.ReadLine();
            string proxyAddress = Sw.ReadLine();
            int proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
            tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
            tmpProxy.Credentials = ProxyCredentials;

            return tmpProxy;
        }

        public void Leaderboard(XmlDocument xmlDoc)
        {
            SqlCommand sqlQuery;
            StringBuilder sb;
            SqlConnection dbConn;
            string dbConnString = ConfigurationManager.AppSettings["sql"];
            string tour = ConfigurationManager.AppSettings["tour"];
            dbConn = new SqlConnection(dbConnString);
            dbConn.Open();

            try
            {
                XmlNodeList xmlEvents = xmlDoc.GetElementsByTagName("Tourn");
                foreach (XmlNode EventNode in xmlEvents)
                {
                    DateTime updated = new DateTime();
                    string tournId = string.Empty;
                    string tournament = string.Empty;
                    string location = string.Empty;
                    string country = string.Empty;
                    int round = 0;
                    int rounds = 4;
                    string status = string.Empty;
                    string type = string.Empty;

                    foreach (XmlAttribute attribute in EventNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "CurTime":
                                updated = DateTime.Parse(attribute.InnerText.Replace(" ET", ""));
                                break;
                            case "TournId":
                                tournId = attribute.InnerText.ToString();
                                break;
                            case "Name":
                                tournament = attribute.InnerText.ToString();
                                break;
                            case "Loc":
                                location = attribute.InnerText.ToString();
                                break;
                            case "LocState":
                                country = attribute.InnerText.ToString();
                                break;
                            case "CurRnd":
                                round = Convert.ToInt32(attribute.InnerText.ToString());
                                break;
                            case "CurRndState":
                                status = attribute.InnerText.ToString();
                                status = FixStatus(status);
                                break;
                            case "Format":
                                type = attribute.InnerText.ToString();
                                break;
                            case "NumRnds":
                                rounds = Convert.ToInt32(attribute.InnerText.ToString());
                                break;
                            default:
                                break;
                        }
                    }

                    XmlNodeList xmlCourses = EventNode.SelectNodes("Courses/Course");
                    foreach (XmlNode courseNode in xmlCourses)
                    {
                        string course = string.Empty;
                        string name = string.Empty;
                        string shortName = string.Empty;
                        int yards = 0;
                        int par = 0;
                        int frontpar = 0;
                        int backpar = 0;
                        int holes = 18;

                        foreach (XmlAttribute attribute in courseNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "CrsYard":
                                    yards = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "CrsPar":
                                    par = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "backPar":
                                    backpar = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "frontPar":
                                    frontpar = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "CrsCode":
                                    course = attribute.InnerText.ToString();
                                    break;
                                case "CrsName":
                                    name = attribute.InnerText.ToString();
                                    break;
                                case "CrsAcronym":
                                    shortName = attribute.InnerText.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboardCourse WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int coursePresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (coursePresent <= 0)
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboardCourse (tour, tournament, course, year, lengthYards, name, shortName, par, frontNinePar, backNinePar, lastUpdated, holes) VALUES (@tour, @tournament, @course, @year, @lengthYards, @name, @shortName, @par, @frontNinePar, @backNinePar, @lastUpdated, @holes)", dbConn);
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                            sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                            sqlQuery.Parameters.Add("@shortName", SqlDbType.VarChar).Value = shortName;
                            sqlQuery.Parameters.Add("@lengthYards", SqlDbType.Int).Value = yards;
                            sqlQuery.Parameters.Add("@par", SqlDbType.Int).Value = par;
                            sqlQuery.Parameters.Add("@frontNinePar", SqlDbType.Int).Value = frontpar;
                            sqlQuery.Parameters.Add("@backNinePar", SqlDbType.Int).Value = backpar;
                            sqlQuery.Parameters.Add("@lastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                            sqlQuery.Parameters.Add("@holes", SqlDbType.Int).Value = holes;
                            sqlQuery.ExecuteNonQuery();
                        }
                    }

                    if (type == "Match")
                    {
                        XmlNodeList xmlRounds = EventNode.SelectNodes("Rounds/Round");
                        sb = new StringBuilder();
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardMatchPlayScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                        sqlQuery.ExecuteNonQuery();
                        foreach (XmlNode RoundNode in xmlRounds)
                        {
                            int curRound = 1;
                            foreach (XmlAttribute attribute in RoundNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "RoundNum":
                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                        {
                                            curRound = Convert.ToInt32(attribute.InnerText);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            XmlNodeList xmlBrackets = RoundNode.SelectNodes("Bracket");
                            foreach (XmlNode BracketNode in xmlBrackets)
                            {
                                int bracket = 0;
                                string bracketName = String.Empty;
                                foreach (XmlAttribute attribute in BracketNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "BracketNum":
                                            if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                            {
                                                bracket = Convert.ToInt32(attribute.InnerText);
                                            }
                                            break;
                                        case "Name":
                                            bracketName = attribute.InnerText.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                XmlNodeList xmlGroups = BracketNode.SelectNodes("Group");
                                int group = 1;
                                foreach (XmlNode GroupNode in xmlGroups)
                                {
                                    int matchNumber = 0;
                                    int holesPlayed = 0;
                                    DateTime teeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                    string course = String.Empty;
                                    string matchStatus = string.Empty;
                                    foreach (XmlAttribute attribute in GroupNode.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "MatchNum":
                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                {
                                                    matchNumber = Convert.ToInt32(attribute.InnerText);
                                                }
                                                break;
                                            case "CrsCode":
                                                course = attribute.InnerText.ToString();
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    int p1PreviousMatch = 0;
                                    string p1Id = string.Empty;
                                    int p1Seed = 0;
                                    string p1FirstName = string.Empty;
                                    string p1LastName = string.Empty;
                                    string p1Country = string.Empty;
                                    int p1Winner = 0;
                                    string p1curStatus = string.Empty;

                                    int p2PreviousMatch = 0;
                                    string p2Id = string.Empty;
                                    int p2Seed = 0;
                                    string p2FirstName = string.Empty;
                                    string p2LastName = string.Empty;
                                    string p2Country = string.Empty;
                                    int p2Winner = 0;
                                    string p2curStatus = string.Empty;

                                    XmlNodeList xmlPlayers = GroupNode.SelectNodes("Player");
                                    int player = 1;
                                    foreach (XmlNode PlayerNode in xmlPlayers)
                                    {
                                        if (player == 2)
                                        {
                                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                                            {
                                                switch (attribute.Name)
                                                {
                                                    case "PrevMatch":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p2PreviousMatch = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "PID":
                                                        p2Id = attribute.InnerText.ToString();
                                                        break;
                                                    case "Lname":
                                                        p2LastName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Fname":
                                                        p2FirstName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Seed":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p2Seed = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "CurMatchScr":
                                                        p2curStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "FinalMatchScr":
                                                        matchStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "MatchWinner":
                                                        if (attribute.InnerText.ToString() == "Yes")
                                                        {
                                                            p2Winner = 1;
                                                        }
                                                        break;
                                                    case "Thru":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            if (attribute.InnerText.ToString() == "F")
                                                            {
                                                                holesPlayed = 18;
                                                            }
                                                            else
                                                            {
                                                                bool result = Int32.TryParse(attribute.InnerText.ToString(), out holesPlayed);
                                                            }
                                                        }
                                                        break;
                                                    case "TTime":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            string tmpTeeTime = attribute.InnerText.ToString();
                                                            if (tmpTeeTime.IndexOf(":") > 0)
                                                            {
                                                                bool isPm = false;
                                                                if (tmpTeeTime.IndexOf("p") >= 0)
                                                                {
                                                                    isPm = true;
                                                                }
                                                                string[] time = tmpTeeTime.Split(' ');
                                                                teeTime = new DateTime(1900, 1, 1, Convert.ToInt32(time[0].Split(':')[0]), Convert.ToInt32(time[0].Split(':')[1]), 0);
                                                                if (teeTime.Hour == 12 && isPm == false)
                                                                {
                                                                    teeTime = teeTime.AddHours(-12);
                                                                }
                                                                else if (isPm && teeTime.Hour < 12)
                                                                {
                                                                    teeTime = teeTime.AddHours(12);
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                                            {
                                                switch (attribute.Name)
                                                {
                                                    case "PrevMatch":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p1PreviousMatch = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "PID":
                                                        p1Id = attribute.InnerText.ToString();
                                                        break;
                                                    case "Lname":
                                                        p1LastName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Fname":
                                                        p1FirstName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Seed":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p1Seed = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "CurMatchScr":
                                                        p1curStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "FinalMatchScr":
                                                        matchStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "MatchWinner":
                                                        if (attribute.InnerText.ToString() == "Yes")
                                                        {
                                                            p1Winner = 1;
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }

                                        player += 1;
                                    }

                                    sb.Append("INSERT INTO golf.dbo.leaderboardMatchPlayScores (tour, tournament, course, year, position, sorting, round, bracket, bracketName, matchNumber, teeTime, holesPlayed, P1PreviousMatch, P1Id, P1Seed, P1FirstName, P1LastName, P1Country, P1Winner, P1Status, P2PreviousMatch, P2Id, P2Seed, P2FirstName, P2LastName, P2Country, P2Winner, P2Status, status) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", 0, " + group + ", " + curRound + ", " + bracket + ", '" + bracketName.Replace("'", "''") + "', " + matchNumber + ", '" + teeTime.ToString("yyyy-MM-dd HH:mm") + "', " + holesPlayed + ", " + p1PreviousMatch + ", '" + p1Id.Replace("'", "''") + "', " + p1Seed + ", '" + p1FirstName.Replace("'", "''") + "', '" + p1LastName.Replace("'", "''") + "', '" + p1Country.Replace("'", "''") + "', " + p1Winner + ", '" + p1curStatus.Replace("'", "''") + "', " + p2PreviousMatch + ", '" + p2Id.Replace("'", "''") + "', " + p2Seed + ", '" + p2FirstName.Replace("'", "''") + "', '" + p2LastName.Replace("'", "''") + "', '" + p2Country.Replace("'", "''") + "', " + p2Winner + ", '" + p2curStatus.Replace("'", "''") + "', '" + matchStatus.Replace("'", "''") + "');");
                                    group += 1;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (leaderboardPresent > 0)
                        {
                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        }
                        else
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                        }
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                        sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                        sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                        sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = rounds;
                        sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                        sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                        sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                        sqlQuery.ExecuteNonQuery();
                    }
                    else
                    {
                        XmlNodeList xmlPlayers = EventNode.SelectNodes("Players/Player");
                        sb = new StringBuilder();
                        StringBuilder sb1 = new StringBuilder();
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                        sqlQuery.ExecuteNonQuery();
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardPlayerRounds WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                        sqlQuery.ExecuteNonQuery();
                        foreach (XmlNode PlayerNode in xmlPlayers)
                        {
                            string id = string.Empty;
                            string firstname = string.Empty;
                            string lastname = string.Empty;
                            string playerCountry = string.Empty;
                            int holesPlayed = 0;
                            string round1ToPar = string.Empty;
                            string round1Strokes = string.Empty;
                            string round2ToPar = string.Empty;
                            string round2Strokes = string.Empty;
                            string round3ToPar = string.Empty;
                            string round3Strokes = string.Empty;
                            string round4ToPar = string.Empty;
                            string round4Strokes = string.Empty;
                            string round5ToPar = string.Empty;
                            string round5Strokes = string.Empty;
                            string round6ToPar = string.Empty;
                            string round6Strokes = string.Empty;
                            string pos = "1000";
                            int tied = 0;
                            string total = string.Empty;
                            string playerStatus = string.Empty;
                            bool insert = true;
                            int sorting = 1;

                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "Fname":
                                        firstname = attribute.InnerText.ToString();
                                        break;
                                    case "Lname":
                                        lastname = attribute.InnerText.ToString();
                                        break;
                                    case "PID":
                                        id = attribute.InnerText.ToString();
                                        break;
                                    case "TournParRel":
                                        total = attribute.InnerText.ToString();
                                        break;
                                    case "CurPos":
                                        try
                                        {
                                            if (attribute.InnerText.ToString().IndexOf("T") >= 0)
                                            {
                                                tied = 1;
                                            }
                                            if (attribute.InnerText.ToString() == "")
                                            {
                                                pos = "1000";
                                            }
                                            else if (attribute.InnerText.ToString() == "0")
                                            {
                                                pos = "1000";
                                            }
                                            else
                                            {
                                                string pattern = @"[^0-9]";
                                                pos = Regex.Replace(attribute.InnerText.ToString(), pattern, "");
                                                if (string.IsNullOrEmpty(pos))
                                                {
                                                    insert = false;
                                                }
                                            }
                                        }
                                        catch (Exception ex1)
                                        {
                                            pos = "1000";
                                        }
                                        break;
                                    case "Thru":
                                        if (attribute.InnerText.ToString() == "F")
                                        {
                                            holesPlayed = 18;
                                        }
                                        else
                                        {
                                            try
                                            {
                                                holesPlayed = Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch
                                            { }
                                        }
                                        break;
                                    case "SortIndex":
                                        try
                                        {
                                            sorting = Convert.ToInt32(attribute.InnerText.ToString());
                                        }
                                        catch
                                        { }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            XmlNodeList xmlRounds = PlayerNode.SelectNodes("Rnd");
                            foreach (XmlNode Round in xmlRounds)
                            {
                                int roundNumber = 0;
                                string roundCourse = string.Empty;
                                string roundToPar = string.Empty;
                                int roundstrokes = 0;
                                int roundStartingTee = 1;
                                int roundHolePlayed = 0;
                                string tmpTeeTime = "1900-01-01";
                                DateTime roundTeeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                foreach (XmlAttribute attribute in Round.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "Num":
                                            try
                                            {
                                                roundNumber = Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch
                                            { }
                                            break;
                                        case "Stroke":
                                            roundstrokes = Convert.ToInt32(attribute.InnerText.ToString());
                                            switch (roundNumber)
                                            {
                                                case 1:
                                                    round1Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 2:
                                                    round2Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 3:
                                                    round3Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 4:
                                                    round4Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 5:
                                                    round5Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 6:
                                                    round6Strokes = attribute.InnerText.ToString();
                                                    break;
                                                default:
                                                    break;
                                            }
                                            break;
                                        case "ParRel":
                                            roundToPar = attribute.InnerText.ToString();
                                            switch (roundNumber)
                                            {
                                                case 1:
                                                    round1ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 2:
                                                    round2ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 3:
                                                    round3ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 4:
                                                    round4ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 5:
                                                    round5ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 6:
                                                    round6ToPar = attribute.InnerText.ToString();
                                                    break;
                                                default:
                                                    break;
                                            }
                                            break;
                                        case "CrsCode":
                                            roundCourse = attribute.InnerText.ToString();
                                            break;
                                        case "Thru":
                                            if (attribute.InnerText.ToString() == "F")
                                            {
                                                roundHolePlayed = 18;
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    roundHolePlayed = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                            }
                                            break;
                                        case "StartTee":
                                            roundStartingTee = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "ttime":
                                            tmpTeeTime += " " + attribute.InnerText.ToString().Replace(" a", "AM").Replace(" p", "PM");
                                            roundTeeTime = DateTime.Parse(tmpTeeTime);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                if (insert)
                                {
                                    sb1.Append("INSERT INTO golf.dbo.leaderboardPlayerRounds (tour, tournament, year, course, player, roundNumber, par, strokes, teeTime, startingTee, matchNumberIndex, matchNumber, holesPlayed, description, lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + roundCourse.Replace("'", "''") + "', '" + id + "', " + roundNumber + ", '" + roundToPar + "', " + roundstrokes + ", '" + roundTeeTime.ToString("yyyy-MM-dd HH:mm") + "', " + roundStartingTee + ", 0, 0, " + roundHolePlayed + ", '', GetDate());");
                                }
                            }

                            if (insert)
                            {
                                sb.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, country, round1topar, round2topar, round3topar, round4topar, round5topar, round6topar, round1strokes, round2strokes, round3strokes, round4strokes, round5strokes, round6strokes, total, holesPlayed) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + pos + "', " + sorting + ", " + tied + ", " + id + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round5ToPar + "', '" + round6ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + round5Strokes + "', '" + round6Strokes + "', '" + total + "', " + holesPlayed + ");");
                                sorting += 1;
                            }
                        }

                        if (!String.IsNullOrEmpty(sb1.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (leaderboardPresent > 0)
                        {
                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        }
                        else
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                        }
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                        sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                        sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                        sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = rounds;
                        sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                        sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                        sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                        sqlQuery.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }

            dbConn.Close();
        }

        private string FixStatus(string status)
        {
            string configOptions = ConfigurationManager.AppSettings["statuses"];
            string tempStatus = status.ToLower();

            Hashtable statuses = new Hashtable();
            string[] options = configOptions.Split(';');
            foreach (string option in options)
            {
                string[] values = option.Split('|');
                statuses.Add(values[0].ToString(), values[1].ToString());
            }

            if (statuses.ContainsKey(tempStatus))
            {
                status = statuses[tempStatus].ToString();
            }

            return status;
        }
    }
}
