﻿com.dstvo.gelf4net Readme
=========================
This package enables logging of GELF formatted messages to disk, amqp and zeromq outputs.
It combines and customizes code from these projects:
Core project code		https://github.com/jjchiw/gelf4net
ZeroMQ Transport		https://bitbucket.org/rgl/log4net.zeromq/src/f702dca73313/log4net.ZeroMQ/

Adding additional fields to log messages
========================================
The code below will add the key-value pair {"myCustomValue" : "WakaWaka"} to the GELF message:

	log4net.ThreadContext.Properties["myCustomValue"] = "WakaWaka";
	log.Error("An Error");

Suggested appender usage
=========================
Developer PC:			Use RollingFileAppender to log to local disk
DEV/QA Windows server:	Use Gelf4NetZeroMQAppender to log to Logstash instance running on environment. Logstash will then forward log events to the appropriate outputs.

An example ZeroMQ appender config:

   <!--This appender logs Gelf formatted messages to a ZeroMQ endpoint, e.g. a Logstash instance running on localhost-->
    <appender name="ZeroMQ" type="Esilog.Gelf4net.Appender.Gelf4NetZeroMQAppender, Esilog.Gelf4net">
      <Address>tcp://127.0.0.1:9995</Address>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%m" />
      </layout>
    </appender>