﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Threading;
using System.Xml;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using log4net;

namespace golfus
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        bool useProxy = Convert.ToBoolean(ConfigurationManager.AppSettings["useProxy"]);
        public string tour = ConfigurationManager.AppSettings["tour"];
        public int statsStartHour = Convert.ToInt16(ConfigurationManager.AppSettings["statsStartHour"]);
        public int statsEndHour = Convert.ToInt16(ConfigurationManager.AppSettings["statsEndHour"]);
        public DateTime statsRun = DateTime.Now.AddDays(-1);
        public string statsLog = ConfigurationManager.AppSettings["statsLog"];
        runInfo curRun;
        ILog log;

        public Service1()
        {
            InitializeComponent();
        }

        internal void TestStartupAndStop(string[] args)
        {
            run();
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += timerElapsed;
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
            //Console.ReadLine();
            Thread.Sleep(600000);
        }

        protected override void OnStart(string[] args)
        {
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        dbConnected = false;
                    }
                }
            }
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        private void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;
            curRun.Errors = new List<string>();

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
            }
            catch (Exception ex)
            {
                dbConnected = false;
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }

            if (dbConnected && dbConn != null)
            {
                try
                {
                    dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }

                try
                {
                    dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                }
            }
            else
            {
                curRun.Errors.Add("Failed to connect to the database");
            }

            // Main leaderboard
            XmlDocument xmlDoc = null;
            string leaderboardXml = string.Empty;
            string scorecardXml = string.Empty;
            tour = ConfigurationManager.AppSettings["tour"];
            if (!String.IsNullOrEmpty(tour))
            {
                leaderboardXml = getLeaderboard();
                if (!String.IsNullOrEmpty(leaderboardXml))
                {
                    xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(leaderboardXml);
                    }
                    catch (Exception ex)
                    {
                        xmlDoc = null;
                        curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }
                }

                if (xmlDoc != null)
                {
                    Leaderboard(xmlDoc);
                }

                scorecardXml = getScorecard();
                xmlDoc = null;
                if (!String.IsNullOrEmpty(scorecardXml))
                {
                    xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(scorecardXml);
                    }
                    catch (Exception ex)
                    {
                        xmlDoc = null;
                        curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }
                }

                if (xmlDoc != null)
                {
                    Scorecard(xmlDoc);
                }
            }

            // Second leaderboard
            tour = ConfigurationManager.AppSettings["tour1"];
            if (!String.IsNullOrEmpty(tour))
            {
                leaderboardXml = getLeaderboard1();
                xmlDoc = null;
                if (!String.IsNullOrEmpty(leaderboardXml))
                {
                    xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(leaderboardXml);
                    }
                    catch (Exception ex)
                    {
                        xmlDoc = null;
                        curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }
                }

                if (xmlDoc != null)
                {
                    Leaderboard(xmlDoc);
                }

                scorecardXml = getScorecard1();
                xmlDoc = null;
                if (!String.IsNullOrEmpty(scorecardXml))
                {
                    xmlDoc = new XmlDocument();
                    try
                    {
                        xmlDoc.LoadXml(scorecardXml);
                    }
                    catch (Exception ex)
                    {
                        xmlDoc = null;
                        curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                    }
                }

                if (xmlDoc != null)
                {
                    Scorecard(xmlDoc);
                }
            }

            // Stats
            tour = ConfigurationManager.AppSettings["tour"];
            if (statsRun.Day != DateTime.Now.Day || ShouldStatsRunNowBecauseItIsMondayMorning())
            {
                Hashtable statCodes = new Hashtable();
                if (ConfigurationManager.AppSettings["statscode-money-list"] != null)
                    statCodes.Add(ConfigurationManager.AppSettings["statscode-money-list"], "money-list");
                if (ConfigurationManager.AppSettings["statscode-fedex-cup"] != null)
                    statCodes.Add(ConfigurationManager.AppSettings["statscode-fedex-cup"], "fedex-cup");

                foreach (DictionaryEntry entry in statCodes)
	            {
	                string statsXml = getStats(entry.Key.ToString());
                    xmlDoc = null;
                    if (!String.IsNullOrEmpty(statsXml))
                    {
                        xmlDoc = new XmlDocument();
                        try
                        {
                            xmlDoc.LoadXml(statsXml);
                        }
                        catch (Exception ex)
                        {
                            xmlDoc = null;
                            curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                        }
                    }

                    if (xmlDoc != null)
                    {
                        Stats(xmlDoc, entry.Value.ToString());
                        statsRun = DateTime.Now;
                        writeStatsLog(entry.Value.ToString());
                    }
	            }
            }

            curRun.End = DateTime.Now;
            endRun();
            writeInfo();

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(300000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private bool ShouldStatsRunNowBecauseItIsMondayMorning()
        {
            var shouldRun = false;
            if (statsStartHour == statsEndHour)
            {
                shouldRun = DateTime.Now.DayOfWeek == DayOfWeek.Monday && DateTime.Now.Hour == statsStartHour;
            }
            else
            {
                shouldRun = DateTime.Now.DayOfWeek == DayOfWeek.Monday && DateTime.Now.Hour >= statsStartHour &&
                            DateTime.Now.Hour < statsEndHour;
            }
            return shouldRun;
        }

        public string getLeaderboard()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=RSyndFullLeaderboard&action=Request&u=supersportfeed&p=" + ConfigurationManager.AppSettings["pgafeedpassword"]);
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public string getLeaderboard1()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=RSyndFullLeaderboard2&action=Request&u=supersportfeed&p=" + ConfigurationManager.AppSettings["pgafeedpassword"]);
                //WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=TSyndFullLeaderboard&action=Request&u=testingfeed&p=testingfeed1");
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public string getScorecard()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=RSyndScorecard&action=Request&u=supersportfeed&p=" + ConfigurationManager.AppSettings["pgafeedpassword"]);
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public string getScorecard1()
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=TSyndScorecard&action=Request&u=testingfeed&p=testingfeed1");
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"windows-1252\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public string getStats(string statsCode)
        {
            string xml = string.Empty;

            try
            {
                WebRequest myRequest = WebRequest.Create("https://feeds.pgatourhq.com/DDManager/DDClient.do?clientFile=YTDStats&action=Request&u=supersportfeed&p=" + ConfigurationManager.AppSettings["pgafeedpassword"] + "&T_CODE=r&STAT_ID=" + statsCode);
                if (useProxy)
                {
                    myRequest.Proxy = returnProxy();
                }
                myRequest.Timeout = 3600000;
                WebResponse myResponse = myRequest.GetResponse();
                StreamReader objStreamReader = new StreamReader(myResponse.GetResponseStream());
                xml = objStreamReader.ReadToEnd();
                objStreamReader.Close();
                myResponse.Close();
                myRequest.Abort();

                xml = xml.Replace("<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>", "<?xml version=\"1.0\"?>");
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
                xml = string.Empty;
            }

            return xml;
        }

        public void Leaderboard(XmlDocument xmlDoc)
        {
            SqlCommand sqlQuery;
            StringBuilder sb;

            try
            {
                XmlNodeList xmlEvents = xmlDoc.GetElementsByTagName("Tourn");
                foreach (XmlNode EventNode in xmlEvents)
                {
                    DateTime updated = new DateTime();
                    string tournId = string.Empty;
                    string tournament = string.Empty;
                    string location = string.Empty;
                    string country = string.Empty;
                    int round = 0;
                    int rounds = 4;
                    string status = string.Empty;
                    string type = string.Empty;

                    foreach (XmlAttribute attribute in EventNode.Attributes)
                    {
                        switch (attribute.Name)
                        {
                            case "CurTime":
                                updated = DateTime.Parse(attribute.InnerText.Replace(" ET.", ""));
                                break;
                            case "TournId":
                                tournId = attribute.InnerText.ToString();
                                break;
                            case "Name":
                                tournament = attribute.InnerText.ToString();
                                break;
                            case "Loc":
                                location = attribute.InnerText.ToString();
                                break;
                            case "LocState":
                                country = attribute.InnerText.ToString();
                                break;
                            case "CurRnd":
                                round = Convert.ToInt32(attribute.InnerText.ToString());
                                break;
                            case "CurRndState":
                                status = attribute.InnerText.ToString();
                                status = FixStatus(status);
                                break;
                            case "Format":
                                type = attribute.InnerText.ToString();
                                break;
                            case "NumRnds":
                                rounds = Convert.ToInt32(attribute.InnerText.ToString());
                                break;
                            default:
                                break;
                        }
                    }

                    if (tournId.StartsWith("0"))
                        tournId = tournId.Substring(1);

                    XmlNodeList xmlCourses = EventNode.SelectNodes("Courses/Course");
                    foreach (XmlNode courseNode in xmlCourses)
                    {
                        string course = string.Empty;
                        string name = string.Empty;
                        string shortName = string.Empty;
                        int yards = 0;
                        int par = 0;
                        int frontpar = 0;
                        int backpar = 0;
                        int holes = 18;

                        foreach (XmlAttribute attribute in courseNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "CrsYard":
                                    yards = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "CrsPar":
                                    par = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "backPar":
                                    backpar = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "frontPar":
                                    frontpar = Convert.ToInt32(attribute.InnerText.ToString());
                                    break;
                                case "CrsCode":
                                    course = attribute.InnerText.ToString();
                                    break;
                                case "CrsName":
                                    name = attribute.InnerText.ToString();
                                    break;
                                case "CrsAcronym":
                                    shortName = attribute.InnerText.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboardCourse WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int coursePresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (coursePresent <= 0)
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboardCourse (tour, tournament, course, year, lengthYards, name, shortName, par, frontNinePar, backNinePar, lastUpdated, holes) VALUES (@tour, @tournament, @course, @year, @lengthYards, @name, @shortName, @par, @frontNinePar, @backNinePar, @lastUpdated, @holes)", dbConn);
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                            sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = name;
                            sqlQuery.Parameters.Add("@shortName", SqlDbType.VarChar).Value = shortName;
                            sqlQuery.Parameters.Add("@lengthYards", SqlDbType.Int).Value = yards;
                            sqlQuery.Parameters.Add("@par", SqlDbType.Int).Value = par;
                            sqlQuery.Parameters.Add("@frontNinePar", SqlDbType.Int).Value = frontpar;
                            sqlQuery.Parameters.Add("@backNinePar", SqlDbType.Int).Value = backpar;
                            sqlQuery.Parameters.Add("@lastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                            sqlQuery.Parameters.Add("@holes", SqlDbType.Int).Value = holes;
                            sqlQuery.ExecuteNonQuery();
                        }
                    }

                    if (type == "Match")
                    {
                        XmlNodeList xmlRounds = EventNode.SelectNodes("Rounds/Round");
                        sb = new StringBuilder();
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardMatchPlayScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                        sqlQuery.ExecuteNonQuery();
                        foreach (XmlNode RoundNode in xmlRounds)
                        {
                            int curRound = 1;
                            foreach (XmlAttribute attribute in RoundNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "RoundNum":
                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                        {
                                            curRound = Convert.ToInt32(attribute.InnerText);
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            XmlNodeList xmlBrackets = RoundNode.SelectNodes("Bracket");
                            foreach (XmlNode BracketNode in xmlBrackets)
                            {
                                int bracket = 0;
                                string bracketName = String.Empty;
                                foreach (XmlAttribute attribute in BracketNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "BracketNum":
                                            if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                            {
                                                bracket = Convert.ToInt32(attribute.InnerText);
                                            }
                                            break;
                                        case "Name":
                                            bracketName = attribute.InnerText.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                XmlNodeList xmlGroups = BracketNode.SelectNodes("Group");
                                int group = 1;
                                foreach (XmlNode GroupNode in xmlGroups)
                                {
                                    int matchNumber = 0;
                                    int holesPlayed = 0;
                                    DateTime teeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                    string course = String.Empty;
                                    string matchStatus = string.Empty;
                                    foreach (XmlAttribute attribute in GroupNode.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "MatchNum":
                                                if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                {
                                                    matchNumber = Convert.ToInt32(attribute.InnerText);
                                                }
                                                break;
                                            case "CrsCode":
                                                course = attribute.InnerText.ToString();
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    int p1PreviousMatch = 0;
                                    string p1Id = string.Empty;
                                    int p1Seed = 0;
                                    string p1FirstName = string.Empty;
                                    string p1LastName = string.Empty;
                                    string p1Country = string.Empty;
                                    int p1Winner = 0;
                                    string p1curStatus = string.Empty;

                                    int p2PreviousMatch = 0;
                                    string p2Id = string.Empty;
                                    int p2Seed = 0;
                                    string p2FirstName = string.Empty;
                                    string p2LastName = string.Empty;
                                    string p2Country = string.Empty;
                                    int p2Winner = 0;
                                    string p2curStatus = string.Empty;

                                    XmlNodeList xmlPlayers = GroupNode.SelectNodes("Player");
                                    int player = 1;
                                    foreach (XmlNode PlayerNode in xmlPlayers)
                                    {
                                        if (player == 2)
                                        {
                                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                                            {
                                                switch (attribute.Name)
                                                {
                                                    case "PrevMatch":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p2PreviousMatch = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "PID":
                                                        p2Id = attribute.InnerText.ToString();
                                                        break;
                                                    case "Lname":
                                                        p2LastName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Fname":
                                                        p2FirstName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Seed":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p2Seed = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "CurMatchScr":
                                                        p2curStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "FinalMatchScr":
                                                        matchStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "MatchWinner":
                                                        if (attribute.InnerText.ToString() == "Yes")
                                                        {
                                                            p2Winner = 1;
                                                        }
                                                        break;
                                                    case "Thru":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            if (attribute.InnerText.ToString() == "F")
                                                            {
                                                                holesPlayed = 18;
                                                            }
                                                            else
                                                            {
                                                                bool result = Int32.TryParse(attribute.InnerText.ToString(), out holesPlayed);
                                                            }
                                                        }
                                                        break;
                                                    case "TTime":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            string tmpTeeTime = attribute.InnerText.ToString();
                                                            if (tmpTeeTime.IndexOf(":") > 0)
                                                            {
                                                                bool isPm = false;
                                                                if (tmpTeeTime.IndexOf("p") >= 0)
                                                                {
                                                                    isPm = true;
                                                                }
                                                                string[] time = tmpTeeTime.Split(' ');
                                                                teeTime = new DateTime(1900, 1, 1, Convert.ToInt32(time[0].Split(':')[0]), Convert.ToInt32(time[0].Split(':')[1]), 0);
                                                                if (teeTime.Hour == 12 && isPm == false)
                                                                {
                                                                    teeTime = teeTime.AddHours(-12);
                                                                }
                                                                else if (isPm && teeTime.Hour < 12)
                                                                {
                                                                    teeTime = teeTime.AddHours(12);
                                                                }
                                                            }
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                                            {
                                                switch (attribute.Name)
                                                {
                                                    case "PrevMatch":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p1PreviousMatch = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "PID":
                                                        p1Id = attribute.InnerText.ToString();
                                                        break;
                                                    case "Lname":
                                                        p1LastName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Fname":
                                                        p1FirstName = attribute.InnerText.ToString();
                                                        break;
                                                    case "Seed":
                                                        if (!String.IsNullOrEmpty(attribute.InnerText.ToString()))
                                                        {
                                                            p1Seed = Convert.ToInt32(attribute.InnerText);
                                                        }
                                                        break;
                                                    case "CurMatchScr":
                                                        p1curStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "FinalMatchScr":
                                                        matchStatus = attribute.InnerText.ToString();
                                                        break;
                                                    case "MatchWinner":
                                                        if (attribute.InnerText.ToString() == "Yes")
                                                        {
                                                            p1Winner = 1;
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                        }

                                        player += 1;
                                    }

                                    if (!String.IsNullOrEmpty(p1FirstName) || !String.IsNullOrEmpty(p1LastName))
                                    {
                                        sb.Append("INSERT INTO golf.dbo.leaderboardMatchPlayScores (tour, tournament, course, year, position, sorting, round, bracket, bracketName, matchNumber, teeTime, holesPlayed, P1PreviousMatch, P1Id, P1Seed, P1FirstName, P1LastName, P1Country, P1Winner, P1Status, P2PreviousMatch, P2Id, P2Seed, P2FirstName, P2LastName, P2Country, P2Winner, P2Status, status) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", 0, " + group + ", " + curRound + ", " + bracket + ", '" + bracketName.Replace("'", "''") + "', " + matchNumber + ", '" + teeTime.ToString("yyyy-MM-dd HH:mm") + "', " + holesPlayed + ", " + p1PreviousMatch + ", '" + p1Id.Replace("'", "''") + "', " + p1Seed + ", '" + p1FirstName.Replace("'", "''") + "', '" + p1LastName.Replace("'", "''") + "', '" + p1Country.Replace("'", "''") + "', " + p1Winner + ", '" + p1curStatus.Replace("'", "''") + "', " + p2PreviousMatch + ", '" + p2Id.Replace("'", "''") + "', " + p2Seed + ", '" + p2FirstName.Replace("'", "''") + "', '" + p2LastName.Replace("'", "''") + "', '" + p2Country.Replace("'", "''") + "', " + p2Winner + ", '" + p2curStatus.Replace("'", "''") + "', '" + matchStatus.Replace("'", "''") + "');");
                                    }
                                    group += 1;
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (leaderboardPresent > 0)
                        {
                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        }
                        else
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                        }
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                        sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                        sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                        sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = rounds;
                        sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                        sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                        sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                        sqlQuery.ExecuteNonQuery();
                    }
                    else
                    {
                        ArrayList countries = new ArrayList();
                        countries.Add("China");
                        countries.Add("Portugal");
                        countries.Add("South Korea");
                        countries.Add("South Africa");
                        countries.Add("England");
                        countries.Add("Scotland");
                        countries.Add("Australia");
                        countries.Add("India");
                        countries.Add("Spain");
                        countries.Add("Canada");
                        countries.Add("Italy");
                        countries.Add("Argentina");
                        countries.Add("Thailand");
                        countries.Add("Netherlands");
                        countries.Add("Denmark");
                        countries.Add("Sweden");
                        countries.Add("Germany");
                        countries.Add("Brazil");
                        countries.Add("Philippines");
                        countries.Add("New Zealand");
                        countries.Add("France");
                        countries.Add("Finland");
                        countries.Add("United States");
                        countries.Add("Ireland");
                        countries.Add("Japan");
                        countries.Add("Chile");

                        XmlNodeList xmlPlayers = EventNode.SelectNodes("Players/Player");
                        sb = new StringBuilder();
                        StringBuilder sb1 = new StringBuilder();
                        sb.Append("DELETE FROM golf.dbo.leaderboardScores WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");
                        sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardPlayerRounds WHERE (tour = '" + tour + "' AND tournament = '" + tournId.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");", dbConn);
                        sqlQuery.ExecuteNonQuery();
                        foreach (XmlNode PlayerNode in xmlPlayers)
                        {
                            string id = string.Empty;
                            string firstname = string.Empty;
                            string lastname = string.Empty;
                            string playerCountry = string.Empty;
                            int holesPlayed = 0;
                            string round1ToPar = string.Empty;
                            string round1Strokes = string.Empty;
                            string round2ToPar = string.Empty;
                            string round2Strokes = string.Empty;
                            string round3ToPar = string.Empty;
                            string round3Strokes = string.Empty;
                            string round4ToPar = string.Empty;
                            string round4Strokes = string.Empty;
                            string round5ToPar = string.Empty;
                            string round5Strokes = string.Empty;
                            string round6ToPar = string.Empty;
                            string round6Strokes = string.Empty;
                            string pos = "1000";
                            int tied = 0;
                            string total = string.Empty;
                            string playerStatus = string.Empty;
                            bool insert = true;
                            int sorting = 1;

                            foreach (XmlAttribute attribute in PlayerNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "Fname":
                                        firstname = attribute.InnerText.ToString();
                                        break;
                                    case "Lname":
                                        lastname = attribute.InnerText.ToString();
                                        break;
                                    case "PID":
                                        id = attribute.InnerText.ToString();
                                        break;
                                    case "TournParRel":
                                        total = attribute.InnerText.ToString();
                                        break;
                                    case "CurPos":
                                        try
                                        {
                                            if (attribute.InnerText.ToString().IndexOf("T") >= 0)
                                            {
                                                tied = 1;
                                            }
                                            if (attribute.InnerText.ToString() == "")
                                            {
                                                pos = "1000";
                                            }
                                            else if (attribute.InnerText.ToString() == "0")
                                            {
                                                pos = "1000";
                                            }
                                            else
                                            {
                                                string pattern = @"[^0-9]";
                                                pos = Regex.Replace(attribute.InnerText.ToString(), pattern, "");
                                                if (string.IsNullOrEmpty(pos))
                                                {
                                                    insert = false;
                                                }
                                            }
                                        }
                                        catch (Exception ex1)
                                        {
                                            pos = "1000";
                                            curRun.Errors.Add(ex1.Message);
                                        }
                                        break;
                                    case "Thru":
                                        if (attribute.InnerText.ToString() == "F")
                                        {
                                            holesPlayed = 18;
                                        }
                                        else
                                        {
                                            try
                                            {
                                                holesPlayed = Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch
                                            { }
                                        }
                                        break;
                                    case "SortIndex":
                                        try
                                        {
                                            sorting = Convert.ToInt32(attribute.InnerText.ToString());
                                        }
                                        catch
                                        { }
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (!String.IsNullOrEmpty(lastname))
                            {
                                playerCountry = isSouthAfrican(firstname, lastname) == true ? "RSA" : "";
                            }

                            if (countries.Contains(lastname))
                                insert = false;

                            XmlNodeList xmlRounds = PlayerNode.SelectNodes("Rnd");
                            foreach (XmlNode Round in xmlRounds)
                            {
                                int roundNumber = 0;
                                string roundCourse = string.Empty;
                                string roundToPar = string.Empty;
                                int roundstrokes = 0;
                                int roundStartingTee = 1;
                                int roundHolePlayed = 0;
                                string tmpTeeTime = "1900-01-01";
                                DateTime roundTeeTime = new DateTime(1900, 1, 1, 0, 0, 0);
                                foreach (XmlAttribute attribute in Round.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "Num":
                                            try
                                            {
                                                roundNumber = Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch
                                            { }
                                            break;
                                        case "Stroke":
                                            roundstrokes = Convert.ToInt32(attribute.InnerText.ToString());
                                            switch (roundNumber)
                                            {
                                                case 1:
                                                    round1Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 2:
                                                    round2Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 3:
                                                    round3Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 4:
                                                    round4Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 5:
                                                    round5Strokes = attribute.InnerText.ToString();
                                                    break;
                                                case 6:
                                                    round6Strokes = attribute.InnerText.ToString();
                                                    break;
                                                default:
                                                    break;
                                            }
                                            break;
                                        case "ParRel":
                                            roundToPar = attribute.InnerText.ToString();
                                            switch (roundNumber)
                                            {
                                                case 1:
                                                    round1ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 2:
                                                    round2ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 3:
                                                    round3ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 4:
                                                    round4ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 5:
                                                    round5ToPar = attribute.InnerText.ToString();
                                                    break;
                                                case 6:
                                                    round6ToPar = attribute.InnerText.ToString();
                                                    break;
                                                default:
                                                    break;
                                            }
                                            break;
                                        case "CrsCode":
                                            roundCourse = attribute.InnerText.ToString();
                                            break;
                                        case "Thru":
                                            if (attribute.InnerText.ToString() == "F")
                                            {
                                                roundHolePlayed = 18;
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    roundHolePlayed = Convert.ToInt32(attribute.InnerText.ToString());
                                                }
                                                catch
                                                { }
                                            }
                                            break;
                                        case "StartTee":
                                            roundStartingTee = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "ttime":
                                            tmpTeeTime += " " + attribute.InnerText.ToString().Replace(" a", "AM").Replace(" p", "PM");
                                            roundTeeTime = DateTime.Parse(tmpTeeTime);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                if (insert)
                                {
                                    sb1.Append("INSERT INTO golf.dbo.leaderboardPlayerRounds (tour, tournament, year, course, player, roundNumber, par, strokes, teeTime, startingTee, matchNumberIndex, matchNumber, holesPlayed, description, lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + roundCourse.Replace("'", "''") + "', '" + id + "', " + roundNumber + ", '" + roundToPar + "', " + roundstrokes + ", '" + roundTeeTime.ToString("yyyy-MM-dd HH:mm") + "', " + roundStartingTee + ", 0, 0, " + roundHolePlayed + ", '', GetDate());");
                                }
                            }

                            if (insert)
                            {
                                sb.Append("INSERT INTO golf.dbo.leaderboardScores (tour, tournament, year, position, sorting, tied, playerId, firstName, lastName, country, round1topar, round2topar, round3topar, round4topar, round5topar, round6topar, round1strokes, round2strokes, round3strokes, round4strokes, round5strokes, round6strokes, total, holesPlayed) VALUES ('" + tour.Replace("'", "''") + "', '" + tournId.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + pos + "', " + sorting + ", " + tied + ", " + id + ", '" + firstname.Replace("'", "''") + "', '" + lastname.Replace("'", "''") + "', '" + playerCountry.Replace("'", "''") + "', '" + round1ToPar + "', '" + round2ToPar + "', '" + round3ToPar + "', '" + round4ToPar + "', '" + round5ToPar + "', '" + round6ToPar + "', '" + round1Strokes + "', '" + round2Strokes + "', '" + round3Strokes + "', '" + round4Strokes + "', '" + round5Strokes + "', '" + round6Strokes + "', '" + total + "', " + holesPlayed + ");");
                                sorting += 1;
                            }
                        }

                        if (!String.IsNullOrEmpty(sb1.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            sqlQuery.ExecuteNonQuery();
                        }

                        sqlQuery = new SqlCommand("SELECT Count(Id) FROM golf.dbo.leaderboard WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        int leaderboardPresent = Convert.ToInt32(sqlQuery.ExecuteScalar());

                        if (leaderboardPresent > 0)
                        {
                            sqlQuery = new SqlCommand("UPDATE golf.dbo.leaderboard SET name = @name, location = @location, country = @country, round = @round, status = @status, updateDate = @updateDate, timeStamp = @timeStamp WHERE (tour = @tour AND tournament = @tournament AND year = @year)", dbConn);
                        }
                        else
                        {
                            sqlQuery = new SqlCommand("INSERT INTO golf.dbo.leaderboard (tour, tournament, year, name, shortname, location, country, round, rounds, status, updateDate, timeStamp, type) VALUES (@tour, @tournament, @year, @name, @shortname, @location, @country, @round, @rounds, @status, @updateDate, @timeStamp, @type)", dbConn);
                        }
                        sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                        sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournId;
                        sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                        sqlQuery.Parameters.Add("@name", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@shortname", SqlDbType.VarChar).Value = tournament;
                        sqlQuery.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                        sqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = country;
                        sqlQuery.Parameters.Add("@round", SqlDbType.Int).Value = round;
                        sqlQuery.Parameters.Add("@rounds", SqlDbType.Int).Value = rounds;
                        sqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = status;
                        sqlQuery.Parameters.Add("@updateDate", SqlDbType.DateTime).Value = DateTime.Now;
                        sqlQuery.Parameters.Add("@timeStamp", SqlDbType.DateTime).Value = updated;
                        sqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                        sqlQuery.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }
        }

        public void Scorecard(XmlDocument xmlDoc)
        {
            SqlCommand sqlQuery;
            StringBuilder sb;
            StringBuilder sb1 = new StringBuilder();
            string player = string.Empty;

            try
            {
                Hashtable courses = new Hashtable();
                
                XmlNodeList xmlPlayers = xmlDoc.GetElementsByTagName("Players");
                foreach (XmlNode playersNode in xmlPlayers)
                {
                    string tournament = string.Empty;
                    if (playersNode.Attributes["TournamentNumber"] != null)
                    {
                        tournament = playersNode.Attributes["TournamentNumber"].InnerText.ToString();
                        if (!(tournament.StartsWith("0")))
                        {
                            tournament = "0" + tournament;
                        }
                    }

                    if (tournament.StartsWith("0"))
                        tournament = tournament.Substring(1);

                    sb1.Append("DELETE FROM golf.dbo.leaderboardPlayerRoundHoles WHERE (tour = '" + tour + "' AND tournament = '" + tournament.Replace("'", "''") + "' AND year = " + DateTime.Now.Year + ");");

                    XmlNodeList xmlPlayer = playersNode.SelectNodes("Player");
                    foreach (XmlNode playerNode in xmlPlayer)
                    {
                        if (playerNode.Attributes["PID"] != null)
                        {
                            player = playerNode.Attributes["PID"].InnerText.ToString();
                        }

                        XmlNodeList xmlRounds = playerNode.SelectNodes("Rnd");
                        foreach (XmlNode roundNode in xmlRounds)
                        {
                            string course = string.Empty;
                            int round = 0;

                            foreach (XmlAttribute attribute in roundNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "Num":
                                        round = Convert.ToInt32(attribute.InnerText.ToString());
                                        break;
                                    case "CrsCode":
                                        course = attribute.InnerText.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            if (!courses.ContainsKey(course))
                            {
                                courses.Add(course, new Hashtable());
                            }

                            XmlNodeList xmlHoles = roundNode.SelectNodes("Hole");
                            foreach (XmlNode holeNode in xmlHoles)
                            {
                                int holeNumber = 0;
                                int holeYards = 0;
                                int holePar = 0;
                                int holeStrokes = 0;
                                bool insert = true;

                                foreach (XmlAttribute attribute in holeNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "Num":
                                            holeNumber = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "ydsOfficial":
                                            holeYards = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "Par":
                                            holePar = Convert.ToInt32(attribute.InnerText.ToString());
                                            break;
                                        case "Score":
                                            try
                                            {
                                                holeStrokes = Convert.ToInt32(attribute.InnerText.ToString());
                                            }
                                            catch { insert = false;  }
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                if (insert)
                                {
                                    sb1.Append("INSERT INTO golf.dbo.leaderboardPlayerRoundHoles (tour, tournament, year, player, round, hole, strokes, putts, gir, fairways, drive, bunkers, lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournament.Replace("'", "''") + "', " + DateTime.Now.Year + ", '" + player + "', " + round + ", " + holeNumber + ", " + holeStrokes + ", 0, 0, 0, 0, 0, GetDate());");
                                }


                                if (!(((Hashtable)courses[course]).ContainsKey(holeNumber)))
                                {
                                    ((Hashtable)courses[course]).Add(holeNumber, holeYards.ToString() + "|" + holePar.ToString());
                                }
                            }
                        }
                    }

                    foreach (DictionaryEntry entry in courses)
                    {
                        sb = new StringBuilder();
                        string course = entry.Key.ToString();
                        Hashtable holes = (Hashtable)entry.Value;

                        foreach (DictionaryEntry hole in holes)
                        {
                            string yards = hole.Value.ToString().Split('|')[0];
                            string par = hole.Value.ToString().Split('|')[1];
                            sb.Append("INSERT INTO golf.dbo.leaderboardCourseHoles (tour, tournament, course, year, hole, par, yards,lastUpdated) VALUES ('" + tour.Replace("'", "''") + "', '" + tournament + "', '" + course.Replace("'", "''") + "', " + DateTime.Now.Year + ", " + hole.Key + ", " + par + ", " + yards + ", Getdate());");
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            sqlQuery = new SqlCommand("SELECT COUNT(Id) FROM golf.dbo.leaderboardCourseHoles WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                            sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournament;
                            sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                            int Holes = Convert.ToInt32(sqlQuery.ExecuteScalar());

                            if (Holes < 18)
                            {
                                sqlQuery = new SqlCommand("DELETE FROM golf.dbo.leaderboardCourseHoles WHERE (tour = @tour AND tournament = @tournament AND year = @year AND course = @course)", dbConn);
                                sqlQuery.Parameters.Add("@tour", SqlDbType.VarChar).Value = tour;
                                sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournament;
                                sqlQuery.Parameters.Add("@course", SqlDbType.VarChar).Value = course;
                                sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = DateTime.Now.Year;
                                sqlQuery.ExecuteNonQuery();

                                sqlQuery = new SqlCommand(sb.ToString(), dbConn);
                                sqlQuery.ExecuteNonQuery();
                            }
                        }
                    }
                }

                if (!String.IsNullOrEmpty(sb1.ToString()) && sb1.ToString().IndexOf("INSERT") >= 0)
                {
                    sqlQuery = new SqlCommand(sb1.ToString(), dbConn);
                    sqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(player + " - " + ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }
        }

        public void Stats(XmlDocument xmlDoc, string urlName)
        {
            SqlCommand sqlQuery;
            string tournament = "us-pga-tour";
            dbConn = new SqlConnection(dbConnString);
            dbConn.Open();
            try
            {
                XmlNode xmlYears = xmlDoc.LastChild.FirstChild.FirstChild;
                if (xmlYears != null)
                {
                    foreach (XmlNode yearNode in xmlYears.ChildNodes)
                    {
                        int year = Convert.ToInt32(yearNode.Attributes["year"].InnerText);
                        if (year > DateTime.Now.Year)
                        {
                            year = DateTime.Now.Year;
                        }
                        string lastTournamentId = string.Empty;
                        string lastTournamentName = string.Empty;
                        DateTime lastTournamentEndDate = DateTime.Now;

                        XmlNodeList tnrNodes = yearNode.SelectNodes("lastTrnProc");
                        foreach (XmlNode tnrNode in tnrNodes)
                        {
                            foreach (XmlAttribute attribute in tnrNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "trnName":
                                        lastTournamentName = attribute.InnerText.ToString();
                                        break;
                                    case "permNum":
                                        lastTournamentId = attribute.InnerText.ToString();
                                        break;
                                    case "endDate":
                                        string tmpDate = attribute.InnerText.ToString();
                                        lastTournamentEndDate = DateTime.Parse(tmpDate);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        XmlNodeList xmlStats = yearNode.SelectNodes("stats/stat");
                        foreach (XmlNode statsNode in xmlStats)
                        {
                            string statId = string.Empty;
                            string statName = string.Empty;
                            string explanation = string.Empty;
                            string average = string.Empty;
                            Dictionary<string, string> dictionary = new Dictionary<string, string>();
                            dictionary.Add("statTitle1", "");
                            dictionary.Add("statTitle2", "");
                            dictionary.Add("statTitle3", "");
                            dictionary.Add("statTitle4", "");
                            dictionary.Add("statTitle5", "");
                            dictionary.Add("statTitle6", "");
                            dictionary.Add("statTitle7", "");

                            foreach (XmlAttribute attribute in statsNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "statID":
                                        statId = attribute.InnerText.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode statNode in statsNode.ChildNodes)
                            {
                                switch (statNode.Name)
                                {
                                    case "statName":
                                        statName = statNode.InnerText.ToString();
                                        break;
                                    case "explanation":
                                        explanation = statNode.InnerText.ToString();
                                        break;
                                    case "tourAvg":
                                        average = statNode.InnerText.ToString();
                                        break;
                                    case "statTitles":
                                        XmlNodeList xmlStatTitles = statNode.ChildNodes;
                                        int count = 1;
                                        foreach (XmlNode statTitle in xmlStatTitles)
                                        {
                                            if (count <= 7)
                                            {
                                                dictionary["statTitle" + count.ToString()] =
                                                    statTitle.InnerText.ToString();
                                                //this.GetType().GetProperty("statTitle" + count.ToString()).SetValue(this, statNode.InnerText.ToString(), null);
                                                count += 1;
                                            }
                                        }
                                        break;
                                    case "details":
                                        XmlNodeList xmlPlayers = statNode.SelectNodes("plr");
                                        int ordering = 1;
                                        string queryText = "DELETE FROM golf.dbo.statsValues WHERE (tournament = '" +
                                                           tournament + "' AND urlName = '" + urlName + "' AND year = " +
                                                           year + " AND statId = '" + statId +
                                                           "' AND lastTournamentId = '" + lastTournamentId + "');";
                                        foreach (XmlNode xmlPlayer in xmlPlayers)
                                        {
                                            int curRank = 1000000;
                                            ;
                                            int curRanktied = 0;
                                            int prevRanktied = 0;
                                            string events = string.Empty;
                                            string prevRank = string.Empty;
                                            string playerNumber = string.Empty;
                                            string playerFirstname = string.Empty;
                                            string playerLastname = string.Empty;
                                            Dictionary<string, string> valueDictionary =
                                                new Dictionary<string, string>();
                                            valueDictionary.Add("statValue1", "");
                                            valueDictionary.Add("statValue2", "");
                                            valueDictionary.Add("statValue3", "");
                                            valueDictionary.Add("statValue4", "");
                                            valueDictionary.Add("statValue5", "");
                                            valueDictionary.Add("statValue6", "");
                                            valueDictionary.Add("statValue7", "");

                                            playerNumber = xmlPlayer.Attributes["plrNum"].InnerText.ToString();

                                            XmlNodeList xmlPlayerData = xmlPlayer.ChildNodes;
                                            foreach (XmlNode xmlData in xmlPlayerData)
                                            {
                                                switch (xmlData.Name)
                                                {
                                                    case "plrName":
                                                        foreach (XmlNode xmlNames in xmlData.ChildNodes)
                                                        {
                                                            switch (xmlNames.Name)
                                                            {
                                                                case "first":
                                                                    playerFirstname = xmlNames.InnerText.ToString();
                                                                    break;
                                                                case "last":
                                                                    playerLastname = xmlNames.InnerText.ToString();
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case "statValues":
                                                        foreach (XmlNode xmlValue in xmlData.ChildNodes)
                                                        {
                                                            switch (xmlValue.Name)
                                                            {
                                                                case "rndEvents":
                                                                    events = xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue1":
                                                                    valueDictionary["statValue1"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue2":
                                                                    valueDictionary["statValue2"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue3":
                                                                    valueDictionary["statValue3"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue4":
                                                                    valueDictionary["statValue4"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue5":
                                                                    valueDictionary["statValue5"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue6":
                                                                    valueDictionary["statValue6"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                case "statValue7":
                                                                    valueDictionary["statValue7"] =
                                                                        xmlValue.InnerText.ToString();
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }
                                                        break;
                                                    case "curRank":
                                                        curRank = Convert.ToInt32(xmlData.InnerText.ToString());
                                                        if (
                                                            String.IsNullOrEmpty(
                                                                xmlData.Attributes["tied"].InnerText.ToString()))
                                                        {
                                                            curRanktied = 0;
                                                        }
                                                        else
                                                        {
                                                            curRanktied = 1;
                                                        }
                                                        break;
                                                    case "prevRank":
                                                        prevRank = xmlData.InnerText.ToString();
                                                        if (
                                                            String.IsNullOrEmpty(
                                                                xmlData.Attributes["tied"].InnerText.ToString()))
                                                        {
                                                            prevRanktied = 0;
                                                        }
                                                        else
                                                        {
                                                            prevRanktied = 1;
                                                        }
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }

                                            queryText +=
                                                "INSERT INTO golf.dbo.statsValues (tournament, urlName, year, statId, lastTournamentId, ordering, number, firstname, lastname, events, currentRank, previousRank, curTied, PrevTied, statValue1, statValue2, statValue3, statValue4, statValue5, statValue6, statValue7, created) VALUES ('" +
                                                tournament + "', '" + urlName + "', " + year + ", '" + statId + "', '" +
                                                lastTournamentId + "', " + ordering + ", '" + playerNumber + "', '" +
                                                playerFirstname.ToString().Replace("'", "''") + "', '" +
                                                playerLastname.ToString().Replace("'", "''") + "', '" + events + "', " +
                                                curRank + ", '" + prevRank + "', " + curRanktied + ", " + prevRanktied +
                                                ", '" + valueDictionary["statValue1"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue2"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue3"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue4"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue5"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue6"].ToString().Replace("'", "''") +
                                                "', '" + valueDictionary["statValue7"].ToString().Replace("'", "''") +
                                                "', GetDate());";
                                            ordering += 1;
                                        }

                                        sqlQuery = new SqlCommand(queryText, dbConn);
                                        sqlQuery.ExecuteNonQuery();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            sqlQuery =
                                new SqlCommand(
                                    "SELECT COUNT(Id) FROM golf.dbo.statsDetails WHERE (tournament = @tournament AND urlName = @urlName AND year = @year AND statId = @statId AND lastTournamentId = @lastTournamentId);",
                                    dbConn);
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournament;
                            sqlQuery.Parameters.Add("@urlName", SqlDbType.VarChar).Value = urlName;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = year;
                            sqlQuery.Parameters.Add("@statId", SqlDbType.VarChar).Value = statId;
                            sqlQuery.Parameters.Add("@lastTournamentId", SqlDbType.VarChar).Value = lastTournamentId;
                            int present = Convert.ToInt32(sqlQuery.ExecuteScalar());

                            if (present > 0)
                            {
                                sqlQuery =
                                    new SqlCommand(
                                        "UPDATE golf.dbo.statsDetails SET tournament = @tournament, urlName = @urlName, year = @year, statId = @statId, statName = @statName, lastTournamentId = @lastTournamentId, lastTournamentName = @lastTournamentName, lastTournamentEndDate = @lastTournamentEndDate, explanation = @explanation, average = @average, updated = @updated, statTitle1 = @statTitle1, statTitle2 = @statTitle2, statTitle3 = @statTitle3, statTitle4 = @statTitle4, statTitle5 = @statTitle5, statTitle6 = @statTitle6, statTitle7 = @statTitle7 WHERE (tournament = @tournament AND urlName = @urlName AND year = @year AND statId = @statId AND lastTournamentId = @lastTournamentId);",
                                        dbConn);
                            }
                            else
                            {
                                sqlQuery =
                                    new SqlCommand(
                                        "INSERT INTO golf.dbo.statsDetails (tournament, urlName, year, statId, statName, lastTournamentId, lastTournamentName, lastTournamentEndDate, explanation, average, updated, statTitle1, statTitle2, statTitle3, statTitle4, statTitle5, statTitle6, statTitle7) VALUES (@tournament, @urlName, @year, @statId, @statName, @lastTournamentId, @lastTournamentName, @lastTournamentEndDate, @explanation, @average, @updated, @statTitle1, @statTitle2, @statTitle3, @statTitle4, @statTitle5, @statTitle6, @statTitle7)",
                                        dbConn);
                            }
                            sqlQuery.Parameters.Add("@tournament", SqlDbType.VarChar).Value = tournament;
                            sqlQuery.Parameters.Add("@urlName", SqlDbType.VarChar).Value = urlName;
                            sqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = year;
                            sqlQuery.Parameters.Add("@statId", SqlDbType.VarChar).Value = statId;
                            sqlQuery.Parameters.Add("@statName", SqlDbType.VarChar).Value = statName;
                            sqlQuery.Parameters.Add("@lastTournamentId", SqlDbType.VarChar).Value = lastTournamentId;
                            sqlQuery.Parameters.Add("@lastTournamentName", SqlDbType.VarChar).Value = lastTournamentName;
                            sqlQuery.Parameters.Add("@lastTournamentEndDate", SqlDbType.VarChar).Value =
                                lastTournamentEndDate;
                            sqlQuery.Parameters.Add("@explanation", SqlDbType.VarChar).Value = explanation;
                            sqlQuery.Parameters.Add("@average", SqlDbType.VarChar).Value = average;
                            sqlQuery.Parameters.Add("@updated", SqlDbType.DateTime).Value = DateTime.Now;
                            sqlQuery.Parameters.Add("@statTitle1", SqlDbType.VarChar).Value =
                                dictionary["statTitle1"].ToString();
                            sqlQuery.Parameters.Add("@statTitle2", SqlDbType.VarChar).Value =
                                dictionary["statTitle2"].ToString();
                            sqlQuery.Parameters.Add("@statTitle3", SqlDbType.VarChar).Value =
                                dictionary["statTitle3"].ToString();
                            sqlQuery.Parameters.Add("@statTitle4", SqlDbType.VarChar).Value =
                                dictionary["statTitle4"].ToString();
                            sqlQuery.Parameters.Add("@statTitle5", SqlDbType.VarChar).Value =
                                dictionary["statTitle5"].ToString();
                            sqlQuery.Parameters.Add("@statTitle6", SqlDbType.VarChar).Value =
                                dictionary["statTitle6"].ToString();
                            sqlQuery.Parameters.Add("@statTitle7", SqlDbType.VarChar).Value =
                                dictionary["statTitle7"].ToString();

                            sqlQuery.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                curRun.Errors.Add(ex.Message + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name + "-" + System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName);
            }
        }

        private bool isSouthAfrican(string firstname, string lastname)
        {
            bool southAfrican = false;

            ArrayList southAfricans = new ArrayList();

            foreach (string SouthAfricanGolfer in ConfigurationManager.AppSettings["SAPlayers"].Split(',')) 
            {
                southAfricans.Add(SouthAfricanGolfer);
            }

            southAfricans.TrimToSize();

            if (southAfricans.Contains(lastname.ToLower()))
            {
                southAfrican = true;
            }

            return southAfrican;
        }

        private WebProxy returnProxy()
        {
            WebProxy tmpProxy = new WebProxy();
            FileStream Fs = new FileStream("C:\\SuperSport\\proxyDetails.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader Sw = new StreamReader(Fs);
            Sw.BaseStream.Seek(0, SeekOrigin.Begin);
            string proxyUsername = Sw.ReadLine();
            string proxyPassword = Sw.ReadLine();
            string proxyDomain = Sw.ReadLine();
            string proxyAddress = Sw.ReadLine();
            int proxyPort = Convert.ToInt32(Sw.ReadLine());
            Sw.Close();
            Fs.Close();
            Fs.Dispose();

            System.Net.NetworkCredential ProxyCredentials = new System.Net.NetworkCredential(proxyUsername, proxyPassword, proxyDomain);
            tmpProxy = new System.Net.WebProxy(proxyAddress, proxyPort);
            tmpProxy.Credentials = ProxyCredentials;

            return tmpProxy;
        }

        private void writeStatsLog(string urlName)
        {
            try
            {
                FileStream Fs = new FileStream(statsLog, FileMode.Append, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Running stats for " + urlName);
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.WriteLine("****************************");
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {

            }
        }
        private void writeInfo()
        {
            try
            {
                FileStream Fs = new FileStream(ConfigurationManager.AppSettings["log"], FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();

                //log.Info("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                //log.Info("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                //foreach (string tmpString in curRun.Errors)
                //{
                //    log.Debug("Error: " + tmpString);
                //}
            }
            catch (Exception ex)
            {

            }
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into golf.dbo.leaderboardIngest (tournament, startTime, endTime, errors, timeStamp) Values (3, @startTime, @endTime, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private string FixStatus(string status)
        {
            string configOptions = ConfigurationManager.AppSettings["statuses"];
            string tempStatus = status.ToLower();

            Hashtable statuses = new Hashtable();
            string[] options = configOptions.Split(';');
            foreach (string option in options)
            {
                string[] values = option.Split('|');
                statuses.Add(values[0].ToString(), values[1].ToString());
            }

            if (statuses.ContainsKey(tempStatus))
            {
                status = statuses[tempStatus].ToString();
            }

            return status;
        }
    }
}